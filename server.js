var express = require('express'),
    request = require('request'),
    extend = require('util')._extend,
    bodyParser = require('body-parser'),
    app = express();

var myLimit = typeof(process.argv[2]) != 'undefined' ? process.argv[2] : '100kb';
console.log('Using limit: ', myLimit);

app.use(bodyParser.json({limit: myLimit}));

app.all('*', function (req, res, next) {

    // Set CORS headers: allow all origins, methods, and headers: you may want to lock this down in a production environment
    res.header("Access-Control-Allow-Origin", req.header('origin'));
    res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
    res.header("Access-Control-Allow-Headers", req.header('access-control-request-headers'));
    res.header("Access-Control-Allow-Credentials","true");
    if (req.method === 'OPTIONS') {
        // CORS Preflight
        res.send();
    } else {
	
	//console.log(req.headers);
	var targetURL = req.url.substr(1);

	if (!targetURL.startsWith('http'))
	{
		targetURL = 'http://'+targetURL;
	}
        //var targetURL = req.header('Target-URL');
        if (!targetURL) {
            res.send(500, { error: 'There is no Target-Endpoint header in the request' });
            return;
        }
	//request({ url: targetURL, method: req.method, json: req.body, headers: {'Authorization': req.header('Authorization')} },
	var freq = {};
	if(req._body)
	{
		freq.json = req.body;
	}
	freq.url = targetURL;
	freq.method = req.method;
	freq.timeout = 4900;
	if(req.header('Authorization'))
	{
		if( !freq.headers )
		{
			freq.headers = {};
		}

		freq.headers.Authorization = req.header('Authorization');
	}	
	console.log( JSON.stringify(freq));
	var cli = request(freq,
            function (error, response, body) {
                if (error) {
                    console.error('error: '+error);// + response.statusCode);
		    //response.close();
                }
            }).pipe(res);
    }
});

app.set('port', process.env.PORT || 1337);

app.listen(app.get('port'), function () {
    console.log('Proxy server listening on port ' + app.get('port'));
});
